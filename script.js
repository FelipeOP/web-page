document.addEventListener("DOMContentLoaded", function() {
    const books = [
        { title: "Book 1", author: "Author 1", year: 2001, description: "Description for Book 1" },
        { title: "Book 2", author: "Author 2", year: 2002, description: "Description for Book 2" },
        { title: "Book 3", author: "Author 1", year: 2003, description: "Description for Book 3" },
        // Add more book objects as needed
    ];

    const bookList = document.getElementById("bookList");
    const searchInput = document.getElementById("searchInput");
    const authorSelect = document.getElementById("authorSelect");

    function displayBooks(filter = {}) {
        bookList.innerHTML = "";
        const filteredBooks = books.filter(book => {
            const matchesAuthor = filter.author ? book.author === filter.author : true;
            const matchesTitle = filter.title ? book.title.toLowerCase().includes(filter.title.toLowerCase()) : true;
            return matchesAuthor && matchesTitle;
        });

        filteredBooks.forEach(book => {
            const bookItem = document.createElement("li");
            bookItem.classList.add("book-item");

            bookItem.innerHTML = `
                <h2>${book.title}</h2>
                <p><strong>Author:</strong> ${book.author}</p>
                <p><strong>Year:</strong> ${book.year}</p>
                <p>${book.description}</p>
            `;

            bookList.appendChild(bookItem);
        });
    }

    searchInput.addEventListener("input", () => {
        const filter = { title: searchInput.value, author: authorSelect.value };
        displayBooks(filter);
    });

    authorSelect.addEventListener("change", () => {
        const filter = { title: searchInput.value, author: authorSelect.value };
        displayBooks(filter);
    });

    displayBooks();
});
